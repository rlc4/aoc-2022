package main

import (
	"bufio"
	"flag"
	"os"

	"aoc2022/internal"
	"aoc2022/internal/day01"
	"aoc2022/internal/day02"
	"aoc2022/internal/day03"
	"aoc2022/internal/day04"
	"aoc2022/internal/day05"
	"aoc2022/internal/day06"
	"aoc2022/internal/day07"
	"aoc2022/internal/day08"
	"aoc2022/internal/day09"
	"aoc2022/internal/day10"
	"aoc2022/internal/day11"
	"aoc2022/internal/day12"
	"aoc2022/internal/day13"
	"aoc2022/internal/day14"
	"aoc2022/internal/day15"
	"aoc2022/internal/day16"
	"aoc2022/internal/day17"
	"aoc2022/internal/day18"
	"aoc2022/internal/day19"
	"aoc2022/internal/day20"
	"aoc2022/internal/day21"
	"aoc2022/internal/day22"
	"aoc2022/internal/day23"
	"aoc2022/internal/day24"
	"aoc2022/internal/day25"
)

var (
	day  *int
	part *int
	file *string
)

var days []internal.Day

func init() {
	day = flag.Int("d", 42, "what day puzzle to run")
	part = flag.Int("p", 42, "which puzzle part to run")
	file = flag.String("f", "No File Provided", "Input file")
	days = []internal.Day{
		day01.Day01{},
		day02.Day02{},
		day03.Day03{},
		day04.Day04{},
		day05.Day05{},
		day06.Day06{},
		day07.Day07{},
		day08.Day08{},
		day09.Day09{},
		day10.Day10{},
		day11.Day11{},
		day12.Day12{},
		day13.Day13{},
		day14.Day14{},
		day15.Day15{},
		day16.Day16{},
		day17.Day17{},
		day18.Day18{},
		day19.Day19{},
		day20.Day20{},
		day21.Day21{},
		day22.Day22{},
		day23.Day23{},
		day24.Day24{},
		day25.Day25{},
	}
}

func main() {
	flag.Parse()

	// Load all of the days into an array for easy reference
	// each day is an interface of a Day which must have Part1 and Part2

	if *file == "No File Provided" {
		panic("I need a file!!!")
	}

	fp, err := os.Open("data/" + *file)
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	scanner := bufio.NewScanner(fp)
	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if *day < 1 || *day > 25 {
		panic("Not a valid day")
	}
	switch *part {
	case 1:
		days[*day-1].Part1(lines)
		break
	case 2:
		days[*day-1].Part2(lines)
		break
	default:
		panic("Part not matched!")
	}
}
