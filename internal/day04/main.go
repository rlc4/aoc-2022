package day04

import (
	"fmt"
	"regexp"
)

type Day04 struct {
}

// var lineReg = regexp.MustCompile(`^(?P<fl>\d)-(?P<fh>\d),(?P<sl>\d)-(P<sh>\d)$`)
var lineReg = regexp.MustCompile(`^(?P<fl>\d)-(?P<fh>\d),(?P<sl>\d)-(?P<sh>\d)$`) // (?P<fh>\d),(?P<sl>\d)-(P<sh>\d)$`)

func (f Day04) Part1(lines []string) {
	// ranges are signified by #-#,#-#
	// count the number of times a range (either side) is entirely included by the other
	// ex:  2-4,4-4 - 4 is covered completely by 2-4
	//      5-6, 4-9 - 5-6 is covered completely be 4-9
	overlaps := 0
	for _, val := range lines {
		var fl, fh, sl, sh = 0, 0, 0, 0

		fmt.Sscanf(val, "%d-%d,%d-%d", &fl, &fh, &sl, &sh)
		// check if we have an overlap
		if fl <= sl && fh >= sh {
			fmt.Println("First surrounds Second - Overlapped!")
			overlaps += 1
		} else if fl >= sl && fh <= sh {
			fmt.Println("Second surrounds First - Overlapped!")
			overlaps += 1
		}
	}
	fmt.Printf("Total overlaps: %d\n", overlaps)
}

func (f Day04) Part2(lines []string) {
	overlaps := 0
	for _, val := range lines {
		var fl, fh, sl, sh = 0, 0, 0, 0
		fmt.Println(val)
		fmt.Sscanf(val, "%d-%d,%d-%d", &fl, &fh, &sl, &sh)

		if fl <= sl && fh >= sh {
			fmt.Println("First surrounds Second - Overlapped!")
			overlaps += 1
		} else if fl >= sl && fh <= sh {
			fmt.Println("Second surrounds First - Overlapped!")
			overlaps += 1
		} else if fl <= sl && fh >= sl {
			fmt.Println("first overlaps the lower of second - Overlapped!")
			overlaps += 1
		} else if fl <= sh && fh >= sh {
			fmt.Println("first overlaps the upper of second - Overlapped!")
			overlaps += 1
		}
	}
	fmt.Println("Total overlaps: ", overlaps)
}
