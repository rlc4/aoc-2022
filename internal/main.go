package internal

type Day interface {
	Part1([]string)
	Part2([]string)
}
