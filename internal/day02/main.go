package day02

import (
	"fmt"
	"strings"
)

type Day02 struct {
}

// takes the strings for player1 and player2
// returns the score p2 receives based on the game logic
func strat1(p1 string, p2 string) (retval int) {

	contest := map[string]map[string]int{
		"A": {"X": 3, "Y": 6, "Z": 0},
		"B": {"X": 0, "Y": 3, "Z": 6},
		"C": {"X": 6, "Y": 0, "Z": 3},
	}
	play := map[string]int{
		"X": 1,
		"Y": 2,
		"Z": 3,
	}

	retval = contest[p1][p2]
	retval += play[p2]
	return
	// return retval
}

func (f Day02) Part1(lines []string) {
	total := 0
	for _, val := range lines {
		player := strings.Split(val, " ")
		game := strat1(player[0], player[1])
		total = total + game
		fmt.Printf("%s vs %s - %3d : %d\n", player[0], player[1], game, total)
	}
	fmt.Println("Final Score: ", total)
}

func strat2(p []string) (retval int) {

	rock := 1
	paper := 2
	scissor := 3

	play := map[string]int{
		"X": 0,
		"Y": 3,
		"Z": 6,
	}
	score := map[string]map[string]int{
		"A": { // opp threw rock
			"X": scissor,
			"Y": rock,
			"Z": paper},
		"B": { // opp threw paper
			"X": rock,
			"Y": paper,
			"Z": scissor},
		"C": { // opp threw scissor
			"X": paper,
			"Y": scissor,
			"Z": rock},
	}

	retval = play[p[1]] + score[p[0]][p[1]]
	return
}

func (f Day02) Part2(lines []string) {
	sum := 0
	for _, val := range lines {
		players := strings.Split(val, " ")
		score := strat2(players)
		sum += score
		fmt.Printf("%+v %d - %d\n", players, score, sum)
	}
	fmt.Println("Final Score: ", sum)
}
