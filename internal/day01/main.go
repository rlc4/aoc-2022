package day01

import (
	"fmt"
	"sort"
	"strconv"
)

type Day01 struct {
}

func (f Day01) Part1(lines []string) {
	var rows []int64
	var sum, max int64
	for _, line := range lines {
		if line == "" {
			sum = 0
			fmt.Println(rows)
			for _, val := range rows {
				sum += val
			}
			if max < sum {
				max = sum
			}
			fmt.Println(sum)
			rows = nil
			continue
		}

		i, err := strconv.ParseInt(line, 10, 0)
		rows = append(rows, i)
		if err != nil {
			fmt.Println("Can't parse this line: ", line)
			panic(err)
		}
	}

	sum = 0
	for _, val := range rows {
		sum += val
	}
	if max < sum {
		max = sum
	}
	fmt.Println("Max Calories: ", max)
}

func sum(nums []int) int {
	var sum int
	sum = 0
	for _, val := range nums {
		sum += val
	}
	return sum
}

func (f Day01) Part2(lines []string) {
	var (
		rows []int
	)
	tops := make([]int, 3, 3)

	for _, line := range lines {
		if line == "" {
			tops = append(tops, sum(rows))
			sort.Ints(tops)
			fmt.Println(tops)
			if len(tops) > 3 {
				tops = tops[1:4]
			}
			rows = nil
			continue
		}
		i, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println("Can't parse this line: ", line)
			panic(err)
		}
		rows = append(rows, i)

	}
	tops = append(tops, sum(rows))
	sort.Ints(tops)
	tops = tops[1:4]

	fmt.Println(tops, sum(tops))
}

/* */
