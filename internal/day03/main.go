package day03

import (
	"fmt"
	"sort"
	"strings"
)

type Day03 struct {
}

func scoreLetter(let string) (ret int) {
	// ascii DEC values:  a - 97 z - 122 A - 65 Z - 90
	// score values a - 1 z - 26 A - 27 Z - 52
	val := []byte(let)[0]
	if val >= 97 && val <= 122 {
		ret = int(val) - 96
	} else if val >= 65 && val <= 90 {
		ret = int(val) - 65 + 27
	} else {
		panic("Letter out of range!")
	}
	return
}

func findDupl(part1 []string, part2 []string) string {
	sort.Strings(part1)
	sort.Strings(part2)
	for {
		if part1[0] == part2[0] {
			return part1[0]
		}
		if part1[0] < part2[0] {
			part1 = part1[1:]
		} else {
			part2 = part2[1:]
		}
		if len(part1) == 0 || len(part2) == 0 {
			panic("Ran out of strings to compare!  No Match Found")
		}
	}
}

func (f Day03) Part1(lines []string) {
	var total int = 0
	for _, val := range lines {
		letters := strings.Split(val, "")
		mid := len(letters) / 2
		dupl := findDupl(letters[:mid], letters[mid:])
		score := scoreLetter(dupl)
		total += score
		fmt.Printf("%-36s -- %18s (%2d) || %18s (%2d) -- %s -- %d\n",
			val,
			strings.Join(letters[:mid], ""),
			len(letters[:mid]),
			strings.Join(letters[mid:], ""),
			len(letters[mid:]),
			dupl,
			score,
		)
	}
	fmt.Println("Total of Priorities: ", total)
}

func findIntersection(lines []string) string {
	var data [3]map[string]string
	// make the maps for insertion
	for i := 0; i < 3; i++ {
		data[i] = make(map[string]string)
	}
	// insert each letter from each string
	for i, val := range lines {
		for _, ltr := range strings.Split(val, "") {
			data[i][ltr] = "exists"
		}
	}
	// compare!
	for key, _ := range data[0] {
		if _, ok := data[1][key]; ok {
			if _, ok := data[2][key]; ok {
				return key
			}
		}
	}
	panic("No matching letter found")
}

func (f Day03) Part2(lines []string) {
	total := 0
	for index := 0; index < len(lines); {
		match := findIntersection(lines[index : index+3])
		total += scoreLetter(match)
		index += 3
	}
	fmt.Println("Total: ", total)

}
