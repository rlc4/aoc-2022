package day05

import (
	"fmt"
	"regexp"
	"strings"
)

type Day05 struct {
}

func topCrates(stacks map[int][]string) (result string) {
	result = ""
	for i := 0; i < len(stacks); i++ {
		result += fmt.Sprintf("%s", stacks[i][len(stacks[i])-1])
	}
	return
}
func showStacks(stacks map[int][]string) {
	var max int = 0
	for i, _ := range stacks {
		if len(stacks[i]) > max {
			max = len(stacks[i])
		}
	}
	for row := max - 1; row >= 0; row-- {
		for i := 0; i < len(stacks); i++ {
			if len(stacks[i])-1 >= row {
				fmt.Printf("[%1s] ", stacks[i][row])
			} else {
				fmt.Printf(" %1s  ", " ")
			}
		}
		fmt.Println()
	}
	for i := 1; i <= len(stacks); i++ {
		fmt.Printf(" %d  ", i)
	}
	fmt.Println()

}

func makeStack(lines []string) (stacks map[int][]string) {
	matcher := regexp.MustCompile(`((\[\w]|\s{3})\s?)`)
	bracket := regexp.MustCompile(`\[(\w+)]`)

	stacks = make(map[int][]string)

	// skip the last line cause it's the column marker that we don't need
	for _, val := range lines[:len(lines)-1] {
		match := matcher.FindAllStringSubmatch(val, -1)
		if len(match) == 0 {
			continue
		}
		for index, box := range match {
			ltr := bracket.FindStringSubmatch(box[0])
			if len(ltr) > 0 {
				stacks[index] = append(stacks[index], ltr[1])
			}
		}
	}
	// stacks are inverted at this point from how we want to use them
	for i, _ := range stacks {
		var ns []string
		for rev := len(stacks[i]) - 1; rev >= 0; rev-- {
			ns = append(ns, stacks[i][rev])
		}
		stacks[i] = ns
	}
	showStacks(stacks)

	return
}

func (f Day05) Part1(lines []string) {
	var (
		stacks        map[int][]string
		stack         []string
		i             int
		qty, from, to int
	)

	for i = 0; lines[i] != ""; i++ {
		stack = append(stack, lines[i])
		// process the stack!
	}
	fmt.Println("stack:\n", strings.Join(stack, "\n"))
	stacks = makeStack(stack)

	for _, val := range lines[i+1:] {
		fmt.Sscanf(val, "move %d from %d to %d\n", &qty, &from, &to)
		fmt.Printf("cmd: %s \tqty: %d from: %d to: %d\n", val, qty, from, to)
		for ; qty > 0; qty-- {
			fmt.Printf(" -- cmd: %s \tqty: %d from: %d to: %d\n", val, qty, from, to)
			stacks[to-1] = append(stacks[to-1], stacks[from-1][len(stacks[from-1])-1])
			stacks[from-1] = stacks[from-1][:len(stacks[from-1])-1]
			showStacks(stacks)
		}
	}

	var crates []string
	for _, stack := range stacks {
		// print the end of each array
		crates = append(crates, stack[len(stack)-1])
	}
	fmt.Println("Answer: ", topCrates(stacks))

	// panic("not implemented")
}

func (f Day05) Part2(lines []string) {
	var (
		stacks        map[int][]string
		stack         []string
		i             int
		qty, from, to int
	)

	for i = 0; lines[i] != ""; i++ {
		stack = append(stack, lines[i])
	}
	stacks = makeStack(stack)
	showStacks(stacks)

	for _, val := range lines[i+1:] {
		fmt.Sscanf(val, "move %d from %d to %d\n", &qty, &from, &to)
		fmt.Printf("cmd: %s \tqty: %d from: %d to: %d\n", val, qty, from, to)
		start := len(stacks[from-1]) - qty
		stop := len(stacks[from-1])
		tmp := stacks[from-1][start:stop]
		stacks[from-1] = stacks[from-1][0:start]
		for i = 0; i < len(tmp); i++ {
			stacks[to-1] = append(stacks[to-1], tmp[i])
		}
		fmt.Printf("stack: %+v  start: %d  stop: %d moving: %+v\n", stacks[from-1], start, stop, tmp)
		showStacks(stacks)
	}
	fmt.Println("Answer: ", topCrates(stacks))

}
