package day06

import (
	"fmt"
	"sort"
	"strings"
)

type Day06 struct {
}

func allUniq(val []string) bool {
	var input = make([]string, len(val))
	copy(input, val) // make sure we don't accidentally modify our array
	sort.Strings(input)
	for i := 0; i < len(input)-1; i++ {
		if input[i] == input[i+1] {
			return false
		}
	}
	return true
}

func findSO(val string, size int) (index int) {
	// var test []string
	if len(val) < 4 {
		panic("Not enough data to find a start-of-packet marker!")
	}
	input := strings.Split(val, "")
	for index = 0; index+size <= len(input); index++ {
		if allUniq(input[index : index+size]) {
			return index + size
		}
	}
	return 0
}

func findSOP(val string) (index int) {
	return findSO(val, 4)
}

func findSOM(val string) (index int) {
	return findSO(val, 14)
}

func (f Day06) Part1(lines []string) {
	result := 0
	for _, val := range lines {
		fmt.Println(val)
		if len(val) > 4 {
			result = findSOP(val)
			fmt.Println("SOP Marker: ", result)
		} else {
			panic("Not enough data to find a start-of-packet marker!")
		}
	}
}

func (f Day06) Part2(lines []string) {
	result := 0
	for _, val := range lines {
		fmt.Println(val)
		if len(val) > 4 {
			result = findSOM(val)
			fmt.Println("SOP Marker: ", result)
		} else {
			panic("Not enough data to find a start-of-packet marker!")
		}
	}
}
